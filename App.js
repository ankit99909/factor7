
import React, {useState, useEffect} from 'react';
import {Dimensions, Image, StyleSheet, View, Text,StatusBar} from 'react-native';
const {height, width} = Dimensions.get('screen');
import 'react-native-gesture-handler';
import Navigator from './screens/navigation';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import AsyncStorage from '@react-native-async-storage/async-storage';





const App = props => {
  const [splasStatus, setSplasStatus] = React.useState(true);



  useEffect(() => {
    if (Platform.OS === 'android') {
      setTimeout(() => {
        setSplasStatus(false);
      }, 3000);
    } else {
      setTimeout(() => {}, 3000);
    }
  }, []);

  const [userAuth, setUserAuth] = useState('');
  useEffect(async () => {
    const userToken = await AsyncStorage.getItem('token');
    setUserAuth(userToken);
    console.log('usertoken ======', userToken);
  }, []);

 
  if (splasStatus === true && Platform.OS === 'android') {
    return (
      <>
        <View style={styles.container}>
        {/* <Image
            source={require('../index/image/logo-1000.jpg')}
            resizeMode="contain"
            style={{height: windowHeight /3.60, width: windowWidth / 1.70,backgroundColor:"#eff1fe",borderRadius:100}}
          /> */}
           {/* <StatusBar 
                        backgroundColor = "#eff1fe"  
                        barStyle = "light"   
                    />   */}
        </View>
      </>
    );
  }

  return (
    <>
      {/* <StatusBar animated={true} backgroundColor="black" /> */}
      <View style={{height: '100%', justifyContent: 'center'}}>
      <Navigator authFlow={userAuth}  />
      </View>
    </>
  );
};
export default App;

const styles = StyleSheet.create({
  container: {
    backgroundColor:"#eff1fe",
    height: height * 1,
    width: width * 1,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
});
