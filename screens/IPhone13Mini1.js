import * as React from "react";
import { Image } from "expo-image";
import { StyleSheet, View,Text,ScrollView,Paragraph,Dimensions ,FlatList,TouchableOpacity} from "react-native";
import { FontFamily, FontSize, Color, Border,logBox } from "../GlobalStyles";
import { Card } from 'react-native-paper';
import Swiper from 'react-native-swiper';


const IPhone13Mini1 = ({navigation}) => {

  const images = [
    'https://images.pexels.com/photos/1414234/pexels-photo-1414234.jpeg?auto=compress&cs=tinysrgb&w=600',
    'https://images.pexels.com/photos/827513/pexels-photo-827513.jpeg?auto=compress&cs=tinysrgb&w=600',
    'https://images.pexels.com/photos/3338681/pexels-photo-3338681.jpeg?auto=compress&cs=tinysrgb&w=600',
  ];
  const cardColors = ['#ff7675', '#74b9ff', '#55efc4', '#ffeaa7', '#a29bfe', '#fd79a8', '#00b894'];

  const data = [
    {
      id: 'item2',
      image: 'https://www.thecococompany.com/cdn/shop/products/Chocolate-Truffle-Pastries_1024x1024.jpg?v=1577958105',
      title: 'Delhi',
      url: 'https://github.com/lehoangnam97/react-native-anchor-carousel',
    },
    {
      id: 'item3',
      image: 'https://bakewithshivesh.com/wp-content/uploads/2021/07/IMG_0745-1638x2048.jpg',
      title: 'mumbai',
      url: 'https://www.npmjs.com/package/react-native-anchor-carousel',
    },
    {
      id: 'item1',
      image: 'https://images.pexels.com/photos/808941/pexels-photo-808941.jpeg?auto=compress&cs=tinysrgb&w=600',
      title: 'punjab',
      url: 'https://www.npmjs.com/package/react-native-anchor-carousel',
    },
    {
      id: 'item6',
      image: 'https://images.pexels.com/photos/291528/pexels-photo-291528.jpeg?auto=compress&cs=tinysrgb&w=600',
      title: 'u.p',
      url: 'https://github.com/lehoangnam97/react-native-anchor-carousel',
    },
    {
      id: 'item4',
      image: 'https://images.pexels.com/photos/1126359/pexels-photo-1126359.jpeg?auto=compress&cs=tinysrgb&w=600',
      title: 'assam',
      url: 'https://github.com/lehoangnam97/react-native-anchor-carousel',
    },
  
    {
      id: 'item5',
      image: 'https://images.pexels.com/photos/887853/pexels-photo-887853.jpeg?auto=compress&cs=tinysrgb&w=600',
      title: 'keral',
      url: 'https://www.npmjs.com/package/react-native-anchor-carousel',
    },
    {
      id: 'item2',
      image: 'https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?auto=compress&cs=tinysrgb&w=600',
      title: 'jammu and kashmir',
      url: 'https://github.com/lehoangnam97/react-native-anchor-carousel',
    },
    
  ];

  return (
    <View style={styles.IPhone13Mini1}>
      <View style={[styles.iphone13Mini1Child]}>
<View style ={[styles.karanEpliainBox,{width:"48%",backgroundColor:"#fafafa",borderRadius:10}]}>
<Text style={styles.karanText}>
  Karan
</Text>
<Text style={styles.evningText}>
  Let,s explore this evening
</Text>
</View>
<View style ={[styles.karanEpliainBox,{flexDirection:"row"}]}>
<Image
            style={[styles.Bitmap_Copy_49]}
            contentFit="cover"
            source={require("../assets/Bitmap_Copy_49.png")}
          />
          <Text style = {styles.offers}>
            Offers
          </Text>
          <Image
            style={[styles.Bitmap_Copy_49]}
            contentFit="cover"
            source={require("../assets/wallet.png")}
          />
             <Text style = {styles. wallet}>
           wallet
          </Text>
</View>
      </View>
      <View style={styles.box2}>
        <Text style={styles.yourtestTest}>Your teste</Text>
<ScrollView style={styles.scrollView}
 showsHorizontalScrollIndicator={false}
 horizontal={true}>
{data.map(({item, index}) => {
  return(
    <Card style={[styles.card, ]}>
    <Card.Cover source={{ uri:"https://images.pexels.com/photos/1157835/pexels-photo-1157835.jpeg?auto=compress&cs=tinysrgb&w=600" }} style={styles.imagecard}/>
    <Card.Content style={{margin:10,height:80,}}>
      <Text variant="titleLarge" style={styles.cartTitle}>CakeZone</Text>
      <Text variant="bodyMedium" style={styles.cartcontent}>Malviya Nagar,</Text>
    </Card.Content>
  </Card>
  )
  })}
</ScrollView>
      </View>
    
      <Swiper
      style={styles.wrapper}
      dot={<View style={styles.dot} />}
      activeDotStyle={styles.activeDotStyle}
    >
      {images.map((image, index) => (
        <View key={index} style={styles.slide}>
          <Image source={{ uri: image }} style={styles.image} />
        </View>
      ))}
    </Swiper>
<View style={{flex:1}}> 
<Text style={styles.yourtestTest}>Popular Ones</Text>
<FlatList
      data={data} 
      renderItem={({item, index}) => {
        return (
          <TouchableOpacity  onPress={()=> navigation.replace('KeepYourSurveySecure')}>
            <View style={styles.CardMainContainer}>
              <View style={[styles.childcard, { width: "34%", }]}>
                <Image source={{ uri: item?.image }} style={styles.imagecard} />
              </View>
              <View style={styles.childcard}>
                <Text style={styles.bakertext}>Laxy Baker</Text>
                <Text style={styles.discriptiontext}>Cakes & Bakes</Text>
                <Text style={styles.discriptiontext}>Malviya Nagar, New Delhi</Text>
                <Text style={styles.Offerstrendingtext}> 4 Offers trending</Text>
                <Text style={styles.Ratingtext}>4.2</Text>
              </View>
            </View>
          </TouchableOpacity>
        );
      }}
    />
</View>
    </View>
  );
};

const styles = StyleSheet.create({
  IPhone13Mini1: {
    flex: 1,
    height: 812,
    overflow: "hidden",
    width: "100%",
    backgroundColor: Color.backgroundWhite,
  },
  iphone13Mini1Child: {
    flexDirection:"row",
    top: 70,
    left: 24,
    borderRadius: 14,
    width:"90%",
    height: "24%",
    // position: "absolute",
    backgroundColor: Color.backgroundWhite,
  },
  karanEpliainBox:{
    height:"34%",
    width:"42%",
    margin:15
  },
  karanText:{
    left:10,
    fontFamily: FontFamily.urbanistSemiBold,
    fontSize:30,
    color:"#8b9ca8"
  },
  evningText:{
    left:10,
    fontFamily: FontFamily.urbanistSemiBold,
    color:"#3b4b56",
    fontSize:18,
  },
  Bitmap_Copy_49:{
    top:2,
    width:60,
    height:60,
    margin:4
  },
  offers:{
    fontSize:17,
    top:74,
    fontFamily: FontFamily.urbanistSemiBold,
    left:14,
    position: "absolute",
    color:"#8e9eaa"
  },
  wallet:{
    top:74,
    fontFamily: FontFamily.urbanistSemiBold,
    left:80,
    position: "absolute",
    color:"#8e9eaa",
    fontSize:17,
  },
  box2:{
    // backgroundColor:"red",
    height:"30%",
    width:"100%"
  },
  scrollView: {
    marginHorizontal: 20,
  
  },
  image:{
    height:100,
    width:300
  },
  card: {
    width: "12%",
    margin:16,
height:"64%",

    // justifyContent:"center",
    // alignItems:"center",
    // Add other styles for the card as needed
  },
  cardContent: {
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    // Add other styles for the title as needed
  },
  content: {
    fontSize: 16,
    // Add other styles for the content as needed
  },
  imagecard:{
    width:"100%",
    height:"20%",
    borderBottomStartRadius:0,
  },
  cartTitle:{
    fontFamily: FontFamily.urbanistBold,
    fontWeight: "700",
    right:10,
    color:"#43535d",
    fontSize:18
  },
  cartcontent:{
    fontFamily: FontFamily.urbanistMedium,
    right:10,
    color:'#a9adad'
  },
  yourtestTest:{
    fontFamily: FontFamily.urbanistBold,
    fontSize:24,
    left:"10%",
    color:"#374752"
   
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: "90%",
    height: 140, // Adjust the height as needed
    borderRadius:20,
  },
  dot: {
    backgroundColor:"#8b9ca8",
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 3,
  },
  activeDotStyle: {
    backgroundColor:  '#374752',
    width: 8,
    height: 8,
    borderRadius: 4,
    margin: 3,
  },
  CardMainContainer:{
    flexDirection:"row",
    // backgroundColor:"red",
    width:"90%",
    height:140,
    margin:10,
    borderRadius:4,
  },
  childcard:{
    height:"90%",
    // backgroundColor:"blue",
    width:"56%",
    margin:10
  },
  imagecard:{
    width:"100%",
    height:"100%",
    borderRadius:10
  },
  bakertext:{
    fontFamily: FontFamily.urbanistBold,
    fontSize:20,
    color:"#374752"
  },
  discriptiontext:{
    fontFamily: FontFamily.urbanistMedium,
    color:"#a2a1a1",
    fontSize:16
  },
  Offerstrendingtext:{
    color:"#e8c4b2",
    fontFamily: FontFamily.urbanistBold,
    fontSize:18,
    right:6
  },
  Ratingtext:{
    fontFamily:FontFamily.urbanistBold,
    color:"#3a4954"
  }
});

export default IPhone13Mini1;
